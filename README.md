# README #

This is the repository for inferring semantic interest profiles from twitter friends, a thesis project at University of Passau. This repository also provides resources referred to in the paper “How Twitter knows you better than your friends do”.

### What is this repository for? ###

* This repository provides resources (python application, evaluation scripts, screenshots, etc.) of the thesis project
* Last updated 09/16/2015
* [Christoph Besel](https://bitbucket.org/beselch/)

### Screenshots of Evaluation ##
The screenshots of the evaluation survey referred by the paper can be found in folder *screenshots* 


### How do I get set up? ###

* First clone the repository
* Install the latest version of Python 3 and all used modules of the Python Package Index (https://pypi.python.org/pypi)
* Get the used resources on corresponding download sites (e.g. http://wibitaxonomy.org/download.jsp)
* Create a twitter developer account and get access token and secret (https://dev.twitter.com/)
* Run the application
* For running the evaluation scripts set up a XAMP environment and create a database for storing the results (see php files for database schema)

### Who do I talk to? ###

* [Christoph Besel](https://bitbucket.org/beselch/)
* Professorship of Media Computer Science at University of Passau (http://www.fim.uni-passau.de/en/media-computer-science/)