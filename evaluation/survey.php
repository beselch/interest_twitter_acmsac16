<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Twitter user study</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<div class="container">
    <div class="page-header">
		<h1>Twitter user study <small>What does your twitter profile says about you?</small></h1>
	</div>
	<div class="container" style="margin-bottom:20px">
	    <img class="img-responsive pull-right" src="logos.png" alt="Logos of the university of passau and twitter">
		<p>For my thesis in Internet Computing (computer science) at University of Passau, Germany <strong>I research on the possibilities of inferring interest profiles</strong> from twitter account information. The purpose of this <strong>user study is to evaluate the quality of the generated user interest profiles</strong>. With this resarch we try to improve technologies for personalized content recommendation while at the same time ensuring users privacy.  <strong>Note:</strong> This study is not conducted by Twitter nor is there any sponsorship or endorsement of this user study by Twitter.</p>
	</div>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Instructions</h3>
		</div>
		<div class="panel-body">
			<p>In the following you see a list of categories representing different fields of interests (activities, group of people, organisations, ...). The categories stand for an abstract concept, not a specific entity.</p>
			<p><strong>Please indicate your strength of interest for each category by selecting the corresponding option (from "very interesting" to "not interesting at all").</strong></p>
		</div>
	</div>
	
	<?php
		$servername = "";
		$username = "";
		$password = "";
		
		$user_id = 0;
		$profile_type_id = 0;
		$profile_generated = 0;
		$survey_completed = 0;

		$screen_name = "";
		
		$START_PROFILE_TYPE_ID = 1;
		$STOP_PROFILE_TYPE_ID = 4;

		// create connection and select db
		$conn = mysqli_connect($servername, $username, $password);
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}

		/* change character set to utf8 */
		if (!mysqli_set_charset($conn, "utf8")) {
		    printf("Error loading character set utf8: %s\n", mysqli_error($conn));
		}
		mysqli_select_db($conn, "evaluation");
		
		// check if a part of the form has already been submitted and store that data to database
		if (isset($_POST["submit"])) {
			foreach($_POST as $key=>$value){
				if(is_numeric($key)) {
					$sql = "UPDATE interest_profiles SET evaluation=$value WHERE id=$key";
					mysqli_query($conn, $sql);
				}
			}
		}
		
		if (isset($_GET["profile_type_id"])) {
			// check if given profile type is within study
			if ($_GET["profile_type_id"] >= $START_PROFILE_TYPE_ID && $_GET["profile_type_id"] <= $STOP_PROFILE_TYPE_ID+1) {
				// check if the last page was called
				if ($_GET["profile_type_id"] == $STOP_PROFILE_TYPE_ID + 1) {
					$profile_type_id = $STOP_PROFILE_TYPE_ID+1;
				} else {
					// check if the given profile type id exists
					$res = mysqli_query($conn, "SELECT * FROM interest_profile_types WHERE id='" . $_GET["profile_type_id"] ."'");
					$row = mysqli_fetch_assoc($res);
					if (!empty($row)) {
						$profile_type_id = $_GET["profile_type_id"];
					}
				}
			}
		}
		
		// get the user id 
		if (isset($_GET["user_id"])) {
			$input_user_id = test_input($_GET["user_id"]);

			// check if user id exits in databse
			$res = mysqli_query($conn, "SELECT * FROM twitter_users WHERE id='$input_user_id'");
			$row = mysqli_fetch_assoc($res);
			if (!empty($row)) {
				$user_id = $row["id"];
				$survey_completed = $row["survey_completed"];
				$profile_generated = $row["profile_generated"];
			} 
		} else if (isset($_GET["screen_name"])) {
			$screen_name = test_input($_GET["screen_name"]);
			
			// get the corresponding user id
			$res = mysqli_query($conn, "SELECT * FROM twitter_users WHERE screen_name='$screen_name'");
			$row = mysqli_fetch_assoc($res);
			if (!empty($row)) {
				$user_id = $row["id"];
				$survey_completed = $row["survey_completed"];
				$profile_generated = $row["profile_generated"];
			}
		}

		
		if($user_id == 0) {
			echo '<div class="alert alert-warning alert" role="alert">';
			echo '<strong>Warning!</strong> No user was provided or the given user id or screenname could not be found in the database. Please check the URL and try again!</div>';
		} else if ($profile_type_id == 0) {
			echo '<div class="alert alert-warning alert" role="alert">';
			echo '<strong>Warning!</strong> No profile type was provided or the given profile type id was not found in the database. Please check the URL and try again!</div>';
		} else if ($survey_completed == 1) {
			echo '<div class="alert alert-success info" role="alert">';
			echo '<strong>Info</strong> You have already completed this survey. Thank you!</div>';
		} else if ($profile_generated == 0) {
			echo '<div class="alert alert-success info" role="alert">';
			echo '<strong>Sorry</strong> Your profile is not ready yet. Please try again later!</div>';
		} else {
			if($profile_type_id <= $STOP_PROFILE_TYPE_ID) {

				if($profile_type_id == 1 || $profile_type_id == 2) {


			
					// get the interest categories of user
					$res = mysqli_query($conn, "SELECT ip.id, ic.interest_category FROM interest_profiles ip JOIN interest_categories ic ON ic.id = ip.interest_category WHERE ip.user_id='$user_id' AND ip.profile_type_id='$profile_type_id'");
					
					// print the interests of the user the input form table
					echo 'Your survey progress:';
					echo '<div class="progress">';
					echo '<div id="progress-bar" class="progress-bar" role="progressbar" aria-valuenow="' . (($profile_type_id-1)/$STOP_PROFILE_TYPE_ID)*100 . '" aria-valuemin="0" aria-valuemax="100" style="width:' . (($profile_type_id-1)/$STOP_PROFILE_TYPE_ID)*100 . '%;">';
					echo '</div></div>';
					
					echo '<div class="jumbotron">';
					echo '<div class="input-group">';
					echo '<form name="evaluation" method="post" action="survey.php?user_id=' . $user_id . '&profile_type_id=' . ($profile_type_id+1) . '">';
					echo '<table class="table table-hover">';
					echo '<thead><tr><th>Interest category</th><th>I consider this category ...</th></tr></thead>';
					echo '<tbody>';
					
					// show the interest categories in a table with evaluation options
					while ($row = mysqli_fetch_assoc($res)) {
						echo '<tr>';
						echo '<td>' . $row["interest_category"] . '</td>';
						echo '<td>';
						
						
						echo '<label class="radio-inline"><input type="radio" name="' . $row["id"] . '" value="4" required />very interesting</label>';
						echo '<label class="radio-inline"><input type="radio" name="' . $row["id"] . '" value="3" required/>interesting</label>';
						echo '<label class="radio-inline"><input type="radio" name="' . $row["id"] . '" value="2" required/>hardly interesting</label>';
						echo '<label class="radio-inline"><input type="radio" name="' . $row["id"] . '" value="1" required/>not interesting at all</label>';
						
						
						/*
						echo '<ul style="list-style-type:none;float:left;display:inline;">';
						echo '<li style="display:inline;">very interesting  </li><li style="display:inline;">   <input type="radio" name="' . $row["id"] . '" value="1" />   </li>';
						echo '<li style="display:inline;"><input type="radio" name="' . $row["id"] . '" value="2" />   </li>';
						echo '<li style="display:inline;"><input type="radio" name="' . $row["id"] . '" value="3" />   </li>';
						echo '<li style="display:inline;"><input type="radio" name="' . $row["id"] . '" value="4" /></li><li style="display:inline;">  not interesting at all</li></ul>';
						*/
						
						echo '</td>';
						echo '</tr>';
					}
					
					echo '</tbody>';
					echo '</table>';
					echo '<input type="submit" name="submit" class="btn btn-primary pull-right" value="Save and Continue">';
					echo '</form>';
					echo '</div>';
					echo '</div>';
				 } else if ($profile_type_id == 3) {
						// print the interest articles for the user in panels
						echo 'Your survey progress:';
						echo '<div class="progress">';
						echo '<div id="progress-bar" class="progress-bar" role="progressbar" aria-valuenow="' . (($profile_type_id-1)/$STOP_PROFILE_TYPE_ID)*100 . '" aria-valuemin="0" aria-valuemax="100" style="width:' . (($profile_type_id-1)/$STOP_PROFILE_TYPE_ID)*100 . '%;">';
						echo '</div></div>';
						
						echo '<div class="jumbotron">';
						echo '<div class="pull-right"><a href="survey.php?user_id=' . $user_id . '&profile_type_id=' . ($profile_type_id+1) . '"" class="btn btn-danger" role="button">I\'m tired, skip this</a></div>';
						echo '<h2>Wikipedia articles</h2>';
						echo '<p>In the following, you will be presented five wikipedia articles. Please indicate your strength of interest in the topcis covered by this articles on the scale beneath.</p><br>';
						echo '<div class="input-group">';
						echo '<form name="evaluation" method="post" action="survey.php?user_id=' . $user_id . '&profile_type_id=' . ($profile_type_id+1) . '">';

				 	// get the interest articles for the user
					$res = mysqli_query($conn, "SELECT ip.id, ic.interest_category, ic.description FROM interest_profiles ip JOIN interest_categories ic ON ic.id = ip.interest_category WHERE ip.user_id='$user_id' AND ip.profile_type_id='$profile_type_id'");
					while ($row = mysqli_fetch_assoc($res)) {
					    
						echo '<div class="panel panel-default">';
						echo '<div class="panel-heading">';
						echo '<h3 class="panel-title">' . substr($row["interest_category"], 5) .'</h3>';
						echo '</div>';
						echo '<div class="panel-body">';
					    echo $row["description"];
					    echo '<br><a target="_blank" href="https://en.wikipedia.org/wiki/' . urlencode(str_replace(' ', '_', substr($row["interest_category"], 5))) . '">Read more on wikipedia</a>';
					    echo '<div class="panel-footer text-right">';
					    echo '<strong>I find this article...  </strong>';
					    echo '<label class="radio-inline"><input type="radio" name="' . $row["id"] . '" value="4" required />very interesting</label>';
						echo '<label class="radio-inline"><input type="radio" name="' . $row["id"] . '" value="3" required/>interesting</label>';
						echo '<label class="radio-inline"><input type="radio" name="' . $row["id"] . '" value="2" required/>hardly interesting</label>';
						echo '<label class="radio-inline"><input type="radio" name="' . $row["id"] . '" value="1" required/>not interesting at all</label>';
						echo '</div></div></div>';
					}

					echo '<input type="submit" name="submit" class="btn btn-primary pull-right" value="Save and Continue">';
					echo '</form></div></div>';
				 } else if ($profile_type_id == 4) {
				 		// get the uninteristing categories of user
						$res = mysqli_query($conn, "SELECT ip.id, ic.interest_category FROM interest_profiles ip JOIN interest_categories ic ON ic.id = ip.interest_category WHERE ip.user_id='$user_id' AND ip.profile_type_id='$profile_type_id' AND weight=0");

				 		// print the interest articles for the user in panels
						echo 'Your survey progress:';
						echo '<div class="progress">';
						echo '<div id="progress-bar" class="progress-bar" role="progressbar" aria-valuenow="' . (($profile_type_id-1)/$STOP_PROFILE_TYPE_ID)*100 . '" aria-valuemin="0" aria-valuemax="100" style="width:' . (($profile_type_id-1)/$STOP_PROFILE_TYPE_ID)*100 . '%;">';
						echo '</div></div>';
						
						echo '<div class="jumbotron">';
						echo '<div class="pull-right"><a href="survey.php?user_id=' . $user_id . '&profile_type_id=' . ($profile_type_id+1) . '"" class="btn btn-danger" role="button">I\'m tired, skip this</a></div>';
						echo '<h2>Does twitter know you better than your friends do?</h2>';

						echo '<p>In the last step we want to find out if our algorithm based on your twitter profile performs better in predicting your interests than your friends do. For this reason you find 10 interest categories beneath, we suppose you are <strong><u>not</u></strong> interested in. First please tell us if we ar correct.</p><br>';
						echo '<div class="input-group">';
						echo '<form name="evaluation" method="post" action="survey.php?user_id=' . $user_id . '&profile_type_id=' . ($profile_type_id+1) . '">';
						echo '<table class="table table-hover">';
						echo '<thead><tr><th>Interest category</th><th>I consider this category ...</th></tr></thead>';
						echo '<tbody>';

						while ($row = mysqli_fetch_assoc($res)) {
							echo '<tr>';
							echo '<td>' . $row["interest_category"] . '</td>';
							echo '<td>';
							
							echo '<label class="radio-inline"><input type="radio" name="' . $row["id"] . '" value="4" required />interesting</label>';
							echo '<label class="radio-inline"><input type="radio" name="' . $row["id"] . '" value="0" required/>not interesting</label>';
							echo '</td>';
							echo '</tr>';
						
						}
				 		echo '</tbody>';
						echo '</table>';
						echo '<input type="hidden" name="friend">';
						echo '<p>On the next (and last) page you will be provided with a link. We would like to ask you to send this link to a friend of yours. He will be asked to assess your interest in about 20 interest categories and by this competes with the results of our algorithm.</p>';
						echo '<input type="submit" name="submit" class="btn btn-primary pull-right" value="Complete survey">';
						echo '</form>';
						echo '</div>';
						echo '</div>';
				 }
			} else if ($profile_type_id == $STOP_PROFILE_TYPE_ID+1) {
				// write complete flag to database
				mysqli_query($conn, "UPDATE twitter_users SET survey_completed=1 WHERE id=" . $user_id);

				// display thank you message
				echo 'Your survey progress:';
				echo '<div class="progress">';
				echo '<div id="progress-bar" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%;">';
				echo '</div></div>';
				echo '<div class="jumbotron">';
				echo '<h1>Thank you!</h1>';
				echo '<p>Thank you so much for taking part in this user study! Your contribution has helped a lot. If you are interested in the results of the user study feel free to <a href="mailto:beselch@fim.uni-passau.de">contact me</a>.</p>';
				
				if(isset($_POST["friend"])) {
					echo '<div class="text-center">';
					echo '<p><strong>Please copy this link and send it to your friend:</p>';
					echo '<p><a href=\"http://example.com/friend.php?user_id=' . $user_id . '\">http://example.com/friend.php?user_id=' . $user_id . '</a></p></strong></div>';
				}

				echo '<p>You can close the tab now.</p>';
				echo '</div>';
			}
		}
				
		function test_input($data) {
		  $data = trim($data);
		  $data = stripslashes($data);
		  $data = htmlspecialchars($data);
		  return $data;
		}
	?>
	<div class="well well-sm"><p>This user study is part of a student thesis at the <a href="http://www.fim.uni-passau.de/en/media-computer-science/" target="_blank">Media Computer Science professorship</a> at <a href="http://www.uni-passau.de/en/" target="_blank">University of Passau</a>, Germany. <a href="http://www.uni-passau.de/en/university/legal-notices/" target="_blank">Legal Notices.</a> 2015.</p><p>All trademarks and registered trademarks are the property of their respective owners. There is no sponsorship or endorsement of this user study by Twitter.</p></div>
	</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>