<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Twitter user study - Registration</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
 <body>
	<div class="container">
    <div class="page-header">
		<h1>Twitter user study <small>What does your twitter profile says about you?</small></h1>
	</div>
	<div class="container" style="margin-bottom:20px">
	    <img class="img-responsive pull-right" src="logos.png" alt="Logos of the university of passau and twitter">
		<p>For my thesis in Internet Computing (computer science) at University of Passau, Germany <strong>I research on the possibilities of inferring interest profiles</strong> from twitter account information. The purpose of this <strong>user study is to evaluate the quality of the generated user interest profiles</strong>. With this resarch we try to improve technologies for personalized content recommendation while at the same time ensuring users privacy.  <strong>Note:</strong> This study is not conducted by Twitter nor is there any sponsorship or endorsement of this user study by Twitter.</p>
	</div>

	<?php
		$servername = "";
		$username = "";
		$password = "";
		
		if (isset($_POST['submit'])) {
			// create connection and select db
			$conn = @mysqli_connect($servername, $username, $password);
			if (!$conn) {
				die('<div class="alert alert-warning alert" role="alert">
					<strong>Warning!</strong>Could not connect to database!</div>');
			}
			mysqli_select_db($conn, "evaluation");
			mysqli_set_charset($conn, "utf8");
			
			$screen_name = mysqli_escape_string($conn, $_POST["screenname"]);

			if (isset($_POST['e-mail'])) {
				$e_mail = mysqli_escape_string($conn, $_POST["e-mail"]);
			} else {
				$e_mail = "NULL";
			}

			$sql = "INSERT INTO twitter_users (screen_name, e_mail) VALUES ('$screen_name', '$e_mail');";
			mysqli_query($conn, $sql);

		echo "<div class=\"jumbotron\">
			<h1>Thank you!</h1>
			<p>Thank you for agreeing to take part in this survey! As soon as your interest profile is generated you will receive a mail with the link to your personal questionnaire. If you didn't provide your mail adresse, save this link and visit the websit in a few days:</p>
			<p><a href=\"http://example.com/survey.php?screen_name=$screen_name&profile_type_id=1\">http://example.com/survey.php?screen_name=$screen_name&profile_type_id=1</a></p>
		</div>";

		} else {
	?>
			<div class="alert alert-warning alert" role="alert">
				<strong>Warning!</strong> There was no form data sent. Plese go back and fill out the form.
			</div>
	
<!-- footer -->
	<div class="well well-sm"><p>This user study is part of a student thesis at the <a href="http://www.fim.uni-passau.de/en/media-computer-science/" target="_blank">Media Computer Science professorship</a> at <a href="http://www.uni-passau.de/en/" target="_blank">University of Passau</a>, Germany. <a href="http://www.uni-passau.de/en/university/legal-notices/" target="_blank">Legal Notices.</a> 2015.</p><p>All trademarks and registered trademarks are the property of their respective owners. There is no sponsorship or endorsement of this user study by Twitter.</p></div>
	</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <?php } ?> 
  </body>
</html>