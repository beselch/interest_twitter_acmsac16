<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Twitter user study</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<div class="container">
    <div class="page-header">
		<h1>Twitter user study <small>What does your twitter profile says about you?</small></h1>
	</div>
	<div class="container" style="margin-bottom:20px">
	    <img class="img-responsive pull-right" src="logos.png" alt="Logos of the university of passau and twitter">
		<p>For my thesis in Internet Computing (computer science) at University of Passau, Germany <strong>I research on the possibilities of inferring interest profiles</strong> from twitter account information. The purpose of this <strong>user study is to evaluate the quality of the generated user interest profiles</strong>. With this resarch we try to improve technologies for personalized content recommendation while at the same time ensuring users privacy.  <strong>Note:</strong> This study is not conducted by Twitter nor is there any sponsorship or endorsement of this user study by Twitter.</p>
	</div>


	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Instructions for friends</h3>
		</div>
		<div class="panel-body">
			<p>Your friend took part in the user study mentioned above. He now wants you to compete against the algorithm by assesing his strenght of interest in the categories listed beneath. We try to answer the question if twitter knows people better than their freinds do. Your participation would help a lot!</p>
			<p><strong>Please indicate the strength of interest you suppose your friend has for each category by selecting the corresponding option (from very interesting to not interesting at all).</strong></p>
		</div>
	</div>
	
	<?php
		$servername = "";
		$username = "";
		$password = "";
		
		$user_id = 0;
		$survey_completed = 1;

		$screen_name = "";
		
		// create connection and select db
		$conn = mysqli_connect($servername, $username, $password);
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}

		/* change character set to utf8 */
		if (!mysqli_set_charset($conn, "utf8")) {
		    printf("Error loading character set utf8: %s\n", mysqli_error($conn));
		}
		mysqli_select_db($conn, "evaluation");
		
		// check if a part of the form has already been submitted and store that data to database
		if (isset($_POST["submit"])) {
			foreach($_POST as $key=>$value){
				if(is_numeric($key)) {
					$sql = "UPDATE interest_profiles SET friend_evaluation=$value WHERE id=$key";
					mysqli_query($conn, $sql);
			}

			}
			// display thank you message
			echo '<div class="jumbotron">';
			echo '<h1>Thank you!</h1>';
			echo '<p>Thank you so much for helping to complete this survey! Your contribution has helped a lot. You could help us even more by taking part in the user study yourself. Find out what your twitter profile says about you!</p>';
			echo '<div class="text-center">';
			echo '<p><strong><a href="register.html" target="_blank"> I want to take part in this user study myself</p></strong></a></div>';					
			echo '<p>You can close the tab now.</p>';
			echo '</div>';
		} else {
				// get the user id 
			if (isset($_GET["user_id"])) {
				$input_user_id = test_input($_GET["user_id"]);

				// check if user id exits in databse
				$res = mysqli_query($conn, "SELECT * FROM twitter_users WHERE id='$input_user_id'");
				$row = mysqli_fetch_assoc($res);
				if (!empty($row)) {
					$user_id = $row["id"];
					$survey_completed = $row["survey_completed"];
				} 
			} else if (isset($_GET["screen_name"])) {
				$screen_name = test_input($_GET["screen_name"]);
				
				// get the corresponding user id
				$res = mysqli_query($conn, "SELECT id, survey_completed FROM twitter_users WHERE screen_name='$screen_name'");
				$row = mysqli_fetch_assoc($res);
				if (!empty($row)) {
					$user_id = $row["id"];
					$survey_completed = $row["survey_completed"];
				}
			}

			
			if($user_id == 0) {
				echo '<div class="alert alert-warning alert" role="alert">';
				echo '<strong>Warning!</strong> No user was provided or the given user id or screenname could not be found in the database. Please check the URL and try again!</div>';
			} else if ($survey_completed == 0) {
				echo '<div class="alert alert-success warning" role="alert">';
				echo '<strong>Warning</strong> Your friend did not complete the survey yet.</div>';
			} else {
				// get the interest categories of user
				$res = mysqli_query($conn, "SELECT ip.id, ic.interest_category FROM interest_profiles ip JOIN interest_categories ic ON ic.id = ip.interest_category WHERE ip.user_id='$user_id' AND ip.profile_type_id=4");
						
				// print the interests of the user the input form table
				echo '<div class="jumbotron">';
					echo '<div class="input-group">';
					echo '<form name="evaluation" method="post" action="friend.php?user_id=' . $user_id . '">';
					echo '<table class="table table-hover">';
					echo '<thead><tr><th>Interest category</th><th>I would say <u>my friend</u> considers this category ...</th></tr></thead>';
					echo '<tbody>';
						
					// show the interest categories in a table with evaluation options
					while ($row = mysqli_fetch_assoc($res)) {
						echo '<tr>';
						echo '<td>' . $row["interest_category"] . '</td>';
						echo '<td>';
						
						
						echo '<label class="radio-inline"><input type="radio" name="' . $row["id"] . '" value="4" required />very interesting</label>';
						echo '<label class="radio-inline"><input type="radio" name="' . $row["id"] . '" value="3" required/>interesting</label>';
						echo '<label class="radio-inline"><input type="radio" name="' . $row["id"] . '" value="2" required/>hardly interesting</label>';
						echo '<label class="radio-inline"><input type="radio" name="' . $row["id"] . '" value="1" required/>not interesting at all</label>';
						
						
						
							
						echo '</td>';
						echo '</tr>';
					}
						
				echo '</tbody>';
				echo '</table>';
				echo '<input type="submit" name="submit" class="btn btn-primary pull-right" value="Complete and send">';
				echo '</form>';
				echo '</div>';
				echo '</div>';	
			}

		}
		
		
				
		function test_input($data) {
		  $data = trim($data);
		  $data = stripslashes($data);
		  $data = htmlspecialchars($data);
		  return $data;
		}
	?>
	<div class="well well-sm"><p>This user study is part of a student thesis at the <a href="http://www.fim.uni-passau.de/en/media-computer-science/" target="_blank">Media Computer Science professorship</a> at <a href="http://www.uni-passau.de/en/" target="_blank">University of Passau</a>, Germany. <a href="http://www.uni-passau.de/en/university/legal-notices/" target="_blank">Legal Notices.</a> 2015.</p><p>All trademarks and registered trademarks are the property of their respective owners. There is no sponsorship or endorsement of this user study by Twitter.</p></div>
	</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>