"""
This module provides functions for linking syntactic input (surface forms) to entities represented by wikipedia pages
and categories.
"""
__author__ = 'Christoph'

import tweepy
import wikipedia
import string
import pickle
import urllib.parse
import operator
from mw.lib import title
from nltk.corpus import stopwords
from nltk import word_tokenize, sent_tokenize, PorterStemmer
import os
import logging
import random

import application.input


logger = logging.getLogger(__name__)

# Constants
OVERLAP_COEFFICIENT_THRESHOLD = 0.07
ENTITY_LINKING_RATING_THRESHOLD = 2.0
PRIOR_SENSE_FILE = "resources" + os.path.sep + "prior_sense.pkl"


# Global variables
global prior_sense_dataset
prior_sense_dataset = None

# Caches
entity_cache_heuristics_on = {}
entity_cache_heuristics_off = {}
candidate_cache = {}
no_auto_wikipedia_page_cache = {}
wikipedia_summary_cache = {}
sense_prior_cache = {}
reverse_linking_cache = {}


def get_candidates(surface_form):
    """Function gets the possible candidate entities for given surface form by using the media wiki
    api autosuggest feature.

    :param input_surface_forms: input surface forms (for instance twitter account names)
    :return: list of possible entity candidates
    """

    candidate_entities = []

    # check cache first
    if surface_form in candidate_cache:
        candidate_entities = candidate_cache[surface_form]
        logger.debug("Fetched entity candidates from cache successfully")
    else:
        try:
            wikipedia_page = wikipedia.page(surface_form, auto_suggest=True)
            candidate_entities.append(wikipedia_page)
        except wikipedia.DisambiguationError as disambiguationError:
            options = disambiguationError.options
            for option in options:
                try:
                    wikipedia_page = wikipedia.page(option)
                    candidate_entities.append(wikipedia_page)
                except Exception as e:
                    logger.debug("The entity cadidates (disambiguation) couldn't be retrieved")
        except Exception as e:
            logger.debug("No entity cadidates (wikipedia pages) could be found")

        # write result to cache
        candidate_cache[surface_form] = candidate_entities

    return candidate_entities


def no_auto_suggest_heuristic(surface_form, wikipedia_page):
    """This functions calculates the quality for an (suggested) entity linking candidate by using a heuristic based
    on the page function of the mediawiki api with diabled auto_suggest parameter.
    The returned rating is a normalized value between 0 and 1.

    :param surface_form: the surface form for which an entity linking is provided
    :param wikipedia_page: the entity which should be linked to the given surface form
    :return: normalized quality rating with value between 0 and 1
    """

    rating = 0

    # check cache first
    if surface_form in no_auto_wikipedia_page_cache:
        rating = no_auto_wikipedia_page_cache[surface_form]
        logger.debug("Fetched no_auto_heuristic from cache successfully")
    else:
        try:
            wikipedia_page_without_auto_suggest = wikipedia.page(title=surface_form, auto_suggest=False)
            if wikipedia_page.pageid == wikipedia_page_without_auto_suggest.pageid:
                rating = 1
        except Exception as e:
            logger.debug("The entity cadidates (wikipedia pages) couldn't be retrieved")

        # write result to cache
        no_auto_wikipedia_page_cache[surface_form] = rating

    return rating


def prior_sense_heuristic(surface_form, wikipedia_page):
    """This functions calculates the quality for an (suggested) entity linking candidate by using a heuristic based
    on the sense prior disambiguation algorithm.
    The returned rating is a normalized value between 0 and 1.

    :param surface_form: the surface form for which an entity linking is provided
    :param wikipedia_page: the entity which should be linked to the given surface form
    :return: normalized quality rating with value between 0 and 1
    """

    rating = 0
    global prior_sense_dataset

    # check cache first
    if surface_form in sense_prior_cache:
        rating = sense_prior_cache[surface_form]
        logger.debug("Fetched sense_prior_heuristic from cache successfully")
    else:
        if prior_sense_dataset:
            rating = prior_sense_disambiguation(surface_form, title.normalize(wikipedia_page.title))
        else:
            # load prior sense
            try:
                with open(PRIOR_SENSE_FILE, "rb") as prior_sense_dataset_file:
                    prior_sense_dataset = pickle.load(prior_sense_dataset_file)
                logger.info("Load the prior sense dataset successfully")

                rating = prior_sense_disambiguation(surface_form, title.normalize(wikipedia_page.title))

                # write result to cache
                sense_prior_cache[surface_form] = rating
            except EnvironmentError as e:
                logger.error("Error while trying to load the prior sense dataset")

    return rating


def prior_sense_disambiguation(surface_form, normalized_wikipedia_title):
    """Function returns the wikipedia page disambiguation for a given surface form based on sense prior algorithm.

    :param surface_form: surface to disambiguate
    :return: prior sense probability
    """
    global prior_sense_dataset

    articles_with_occurence = []
    prior_sense_result = 0

    for article_key in prior_sense_dataset:
        for word_key in prior_sense_dataset[article_key]:
            if word_key == surface_form.lower().strip():
                articles_with_occurence.append(
                    (title.normalize(urllib.parse.unquote(article_key, encoding="utf-8")),
                     prior_sense_dataset[article_key][word_key]))

    if len(articles_with_occurence) > 0:
        # calculate max number of occurrences
        max_number_of_occurrences = max(articles_with_occurence, key=lambda x: x[1])[1]

        # get number of occurrences for wikipedia page
        number_of_occurrences = 0
        for article in articles_with_occurence:
            if article[0] == normalized_wikipedia_title:
                number_of_occurrences = article[1]

        prior_sense_result = number_of_occurrences / max_number_of_occurrences

    return prior_sense_result


def overlap_coefficient_heuristic(screenname, wikipedia_page):
    """This functions calculates the quality for an (suggested) entity linking candidate by using a heuristic based
    on the overlap coefficient between to contexts.
    The returned rating is a normalized value between 0 and 1.

    :param screenname: the screenname form for which an entity linking is provided
    :param wikipedia_page: the entity which should be linked to the given surface form
    :return: normalized quality rating with value between 0 and 1
    """

    rating = 0

    if wikipedia_page.pageid in wikipedia_summary_cache:
        wikipedia_summary = wikipedia_summary_cache[wikipedia_page.pageid]
        logger.debug("Fetched wikipedia summary from cache successfully")
    else:
        wikipedia_summary = wikipedia_page.summary

    normalized_user_context = normalize_text(application.input.get_context(screenname))
    normalized_wiki_context = normalize_text(wikipedia_summary)

    if len(normalized_user_context) > 0 and len(normalized_wiki_context) > 0:
        overlap_coefficient = (
            len((normalized_user_context.intersection(normalized_wiki_context))) / min(len(normalized_user_context),
                                                                                       len(normalized_wiki_context)))
        if overlap_coefficient > OVERLAP_COEFFICIENT_THRESHOLD:
            rating = 1
        else:
            rating = overlap_coefficient / OVERLAP_COEFFICIENT_THRESHOLD

    logger.debug("Calculated overlap coefficient successfully")
    return rating


def normalize_text(text):
    """
    Normalizes a given text to prepare for comparison.

    :param text: text to compare
    :return: normalized text as set of tokens
    """

    # tokenize senteces
    sentences = sent_tokenize(text)

    # tokenize words
    tokens = []
    for sentence in sentences:
        tokens.extend(word_tokenize(sentence))

    # remove stopwords, urls, punctuation and other noise
    tokens = [token for token in tokens if not (token.startswith("http") or token.startswith("//") or
                                                token in stopwords.words("english") or
                                                token in list(string.punctuation + "''" + "``" + "-" + "...") or
                                                token.startswith("'"))]

    # remove hashtag symbols
    tokens = [token[1:] if token.startswith("#") else token for token in tokens]

    # stem tokens
    porter_stemmer = PorterStemmer()
    tokens = [porter_stemmer.stem(t) for t in tokens]

    # convert tokens to set (duplicates are removed here!)
    tokens = set(tokens)

    return tokens


def reverse_linking_heuristic(user_id, wikipedia_page):
    """This functions calculates the quality for an (suggested) entity linking candidate by using a heuristic based
    on the possibility to do a reverse linking with the twitter search api.
    The returned rating is a normalized value between 0 and 1.

    :param user_id: the user id of the surface form user
    :param wikipedia_page: the entity which should be linked to the given surface form
    :return: normalized quality rating with value between 0 and 1
    """

    rating = 0

    # first check cache
    if user_id in reverse_linking_cache:
        rating = reverse_linking_cache[user_id]
        logger.debug("Fetched reverse linking from cache successfully")
    else:
        if application.input.reverse_linking(wikipedia_page.title, user_id):
            rating = 1
        # write result to cache
        reverse_linking_cache[user_id] = rating
        logger.debug("Reverse linking successful")

    return rating


def link_entities(username, heuristics_on=True):
    """Function returns the interest entities represented by wikipedia pages to a given username.

    :param username: the username of the user for whom the interest profile should be created
    :return: list of interest entities (represented as wikipedia pages) linked to user profile
    """
    entities = []

    # check cache first
    if heuristics_on:
        if username in entity_cache_heuristics_on:
            entities = entity_cache_heuristics_on[username]
            logger.info("Fetched entity link from cache successfully")
    else:
        if username in entity_cache_heuristics_off:
            entities = entity_cache_heuristics_off[username]
            logger.info("Fetched entity link from cache successfully")

    # if cache is empty, use web APIs
    if len(entities) == 0:
        # get surface form and possible entity linking candidates
        surface_forms = application.input.get_surface_forms(username)

        for surface_form in surface_forms:
            entity_candidates = get_candidates(surface_form.name)

            if len(entity_candidates) > 0:
                if heuristics_on:
                    rated_entites = []
                    for entity_candidate in entity_candidates:
                        # calculate the rating values
                        rating = no_auto_suggest_heuristic(surface_form.name, entity_candidate) +\
                                 reverse_linking_heuristic(surface_form.id, entity_candidate) +\
                                 overlap_coefficient_heuristic(surface_form.screen_name, entity_candidate) +\
                                 prior_sense_heuristic(surface_form.name, entity_candidate)
                        rated_entites.append((entity_candidate, rating))

                        logger.info((entity_candidate, rating))

                    # sort entities by rating
                    rated_entites.sort(key=lambda x: x[1])
                    if rated_entites[0][1] >= ENTITY_LINKING_RATING_THRESHOLD:
                        entities.append(rated_entites[0][0])
                else:
                    # entity linking is not activated set unambiguous page or random page of ambiguous pages as entity
                    if len(entity_candidates) == 1:
                        entities.append(entity_candidates[0])
                    elif len(entity_candidates) > 1:
                        entities.append(random.choice(entity_candidates))

        # write result to correct cache
        if heuristics_on:
            entity_cache_heuristics_on[username] = entities
        else:
            entity_cache_heuristics_off[username] = entities

    # return result
    return entities
