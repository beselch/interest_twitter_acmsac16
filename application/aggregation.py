__author__ = 'Christoph'

import os
import networkx as nx
import math
import requests
import wikipedia
from mw.lib import title
import logging

import application.entity_linking


logger = logging.getLogger(__name__)

# Constants
CATEGORY_TAXONOMY_FILE = "resources" + os.path.sep + "wibi_category_taxonomy.txt"
NUMBER_OF_ITERATIONS = 5
DECAY_FACTOR = 0.2

# Global variables
global category_taxonomy
category_taxonomy = None

# Caches
category_cache = {}
category_size_cache = {}


def get_categories(entities):
    """Function gets the wikipedia categories assigned to the entities.

    :param entities: entities in form of wikipedia pages
    :return: list of all entities with the categories associated with them
    """
    result = []

    for entity in entities:
        categories = []

        if entity.pageid in category_cache:
            categories.extend(category_cache[entity.pageid])
        else:
            try:
                entity_categories = entity.categories
                categories.extend(entity_categories)
                category_cache[entity.pageid] = entity_categories
            except Exception as e:
                logger.debug("Wikipedia categories could not be retrieved")

        result.append((entity, categories))

    return result


def generate_category_taxonomy():
    """Generates the category taxonomy used for performing the spreading activation algorithm
    """

    global category_taxonomy
    try:
        category_taxonomy = nx.read_edgelist(CATEGORY_TAXONOMY_FILE, delimiter="\t", nodetype=str,
                                             create_using=nx.DiGraph())
    except Exception as e:
        logger.error("An error occurred while loading the taxonomy file")


def initial_activation(entities_with_categories, page_normalization=True, intersection_boost_on=False):
    """Function activates all categories of entities/wikipedia pages initially (so called explicit interests, which
    are directly inferred from the entities).

    :param entities_with_categories: the wikipedia entities and the categories assigned to the entities
    """
    global category_taxonomy

    for element in entities_with_categories:
        entity = element[0]
        categories = element[1]

        # search for input
        for category in categories:

            if category_taxonomy.has_node(category):
                if page_normalization:
                    # calculate penalty based on ingoing pages
                    penalty = 0
                    number_of_pages = get_category_size(category)["number_of_pages"]

                    if number_of_pages > 1:
                        if 1 / math.log10(number_of_pages) > 1:
                            penalty = 0
                        else:
                            penalty = 1 / math.log10(number_of_pages)

                    weight = 1 * penalty
                else:
                    weight = 1

                # activate nodes with initial activation (including calculated penalty if activated)
                if "activation" not in category_taxonomy.node[category]:
                    category_taxonomy.node[category]["activation"] = weight
                else:
                    category_taxonomy.node[category]["activation"] += weight

                # save activating entities for later intersection boost
                if "entities" not in category_taxonomy.node[category]:
                    category_taxonomy.node[category]["entities"] = set(entity.pageid)
                else:
                    category_taxonomy.node[category]["entities"].add(entity.pageid)

        if intersection_boost_on:
            # calculate the intersection boost
            # get all activated nodes first
            activated_nodes = []
            for node in category_taxonomy.nodes_iter(data=True):
                if bool(node[1]):
                    activated_nodes.append(node)

            # get the maximum number of entities activating a node
            max_number_of_activating_entities = 0
            for node in activated_nodes:
                if len(node[1]["entities"]) > max_number_of_activating_entities:
                    max_number_of_activating_entities = len(node[1]["entities"])

            # add intersection boost to initial activation weight of category nodes
            for node in activated_nodes:
                number_of_activating_entities = len(node[1]["entities"])
                node[1]["activation"] *= (number_of_activating_entities/max_number_of_activating_entities)

    logger.info("Initial activation with page_normalization (" + str(page_normalization) + ") and intersection_boost ("
                + str(intersection_boost_on) + ") successful")


def clean_up_taxonomy():
    """This functions cleans the category taxonomy by removing the activation from all nodes, so the taxonomy is
    reset.
    """

    global category_taxonomy

    for node in category_taxonomy.nodes_iter(data=True):
        if "entities" in node[1]:
            node[1].pop("entities", None)
        if bool(node[1]):
            node[1].clear()

    logger.info("Cleaned up taxonomy successfully")


def spreading_activation(number_of_iterations=NUMBER_OF_ITERATIONS, decay_factor=DECAY_FACTOR, normalization_on=True,
                         intersection_boost_on=False):
    """
    Performs the activation spreading algorithm on the category taxonomy.

    :param number_of_iterations: the number of acitvation waves/iterations
    :param decay_factor: the decay factor (reducing spread activation from iteration to iteration)
    """
    global category_taxonomy

    # start spreading activation from initial activated nodes
    activated_nodes = []
    for i in range(1, number_of_iterations):
        # get all activated nodes
        for node in category_taxonomy.nodes_iter(data=True):
            if bool(node[1]):
                activated_nodes.append(node)

        if intersection_boost_on:
            # get the maximum number of entities activating a node
            max_number_of_activating_entities = 0
            for node in activated_nodes:
                if len(node[1]["entities"]) > max_number_of_activating_entities:
                    max_number_of_activating_entities = len(node[1]["entities"])

        # iterate over all activated nodes an spread activation
        for node in activated_nodes:
            successors = category_taxonomy.successors(node[0])
            for successor in successors:
                penalty = 1
                intersection_boost = 1

                if intersection_boost_on:
                    # update activating entities for intersection boost
                    if "entities" not in category_taxonomy.node[successor]:
                        category_taxonomy.node[successor]["entities"] = node[1]["entities"]
                    else:
                        category_taxonomy.node[successor]["entities"].update(node[1]["entities"])

                if normalization_on:
                    # calculate the penalty for number of ingoing edges
                    if (1 / (math.log10(category_taxonomy.in_degree(successor) + 1))) > 1:
                        penalty = 1
                    else:
                        penalty = 1 / (math.log10(category_taxonomy.in_degree(successor) + 1))

                if intersection_boost_on:
                    # calculate the intersection boost
                    number_of_activating_entities = len(category_taxonomy.node[successor]["entities"])
                    if number_of_activating_entities > max_number_of_activating_entities:
                        max_number_of_activating_entities = number_of_activating_entities
                    intersection_boost = number_of_activating_entities / max_number_of_activating_entities

                # updated activation of connected nodes
                if "activation" not in category_taxonomy.node[successor]:
                    # if connected node is no activated yet add an activation to the node
                    category_taxonomy.node[successor]["activation"] = decay_factor * node[1]["activation"] \
                                                                      * penalty * intersection_boost
                else:
                    # if node has already a activation add the new ingoing activation to the current activation
                    category_taxonomy.node[successor]["activation"] += decay_factor * node[1]["activation"] \
                                                                       * penalty * intersection_boost

    logger.info("Spreading activation with " + str(number_of_iterations) + " iterations and decay factor "
                + str(decay_factor) + " successful")


def get_top_interests(top=10, filter_by=[], get_all=False):
    """This function gets the top interests by returning the most activated nodes of the category taxonomy.

    :param top: n-best nodes/interests to return
    :return: the n-best interest/most activated nodes of the categor
    """
    global category_taxonomy
    top_interests = []

    if top > 0:
        # get all activated nodes
        activated_nodes = []
        for node in category_taxonomy.nodes_iter(data=True):
            if bool(node[1]):
                activated_nodes.append(node)

        # sort the activated nodes by activation
        activated_nodes.sort(key=lambda k: k[1]["activation"], reverse=True)

        # calculate the total network activation for the top x nodes
        total_activation = 0
        for node in activated_nodes[:top]:
            total_activation += node[1]["activation"]

        # get the names of the most activated categories (top x) and their normalized weight
        interests = []
        for node in activated_nodes:
            interests.append((node[0], node[1]["activation"] / total_activation))

        # filter interests if necessary
        if len(filter_by) > 0:
            interests = list(filter(lambda k: k[0] not in filter_by, interests))

        # return all interests or top slice of them
        if get_all:
            top_interests = interests
        else:
            if top <= len(interests):
                top_interests = interests[:top]
            else:
                top_interests = interests

    return top_interests


def infer_interests(username, entity_heuristics=True, number_of_iterations=NUMBER_OF_ITERATIONS,
                    decay_factor=DECAY_FACTOR, normalization=True, page_intersection_boost=False,
                    category_itersection_boost=True):
    """Functions infers the interest of a given user by performing a activation spreading on the wikipedia category
    taxonomy. Output is the weighted category taxonomy, which can be accesed by get_top_interests function.

    :param username: name of the user
    """
    global category_taxonomy

    entities = application.entity_linking.link_entities(username, heuristics_on=entity_heuristics)
    entities_with_categories = get_categories(entities)

    if category_taxonomy:
        clean_up_taxonomy()
        initial_activation(entities_with_categories, page_normalization=normalization,
                           intersection_boost_on=page_intersection_boost)
        spreading_activation(number_of_iterations=number_of_iterations, decay_factor=decay_factor,
                             normalization_on=normalization, intersection_boost_on=category_itersection_boost)
    else:
        generate_category_taxonomy()
        initial_activation(entities_with_categories, page_normalization=normalization,
                           intersection_boost_on=page_intersection_boost)
        spreading_activation(number_of_iterations=number_of_iterations, decay_factor=decay_factor,
                             normalization_on=normalization, intersection_boost_on=category_itersection_boost)


def get_category_size(category):
    """Function returns the number of pages and the number of subcategories to a given category.

    :param category: The plain text name of the category
    :return: a dictionary that contains the number of pages and number of subcategories for the given category
             if retrieving the numbers wasn't successful the numbers are set to -1
    """

    number_of_pages = -1
    number_of_subcategories = -1

    # check cache first
    if category in category_size_cache:
        result = category_size_cache[category]
    else:
        try:
            query_params = dict(action='query', format='json', prop='categoryinfo',
                                titles="Category:" + title.normalize(category))

            request = requests.get(wikipedia.API_URL, params=query_params,
                                   headers={"User-Agent": wikipedia.USER_AGENT}).json()
            category_id = [page for page in request["query"]["pages"].keys()][0]
            number_of_pages = request["query"]["pages"][category_id]["categoryinfo"]["pages"]
            number_of_subcategories = request["query"]["pages"][category_id]["categoryinfo"]["subcats"]
            logger.debug("The category page number was retrieved successfully")
        except Exception as e:
            logger.debug("The number of pages for a category could not be retrieved")

        result = dict(number_of_pages=number_of_pages, number_of_subcategories=number_of_subcategories)
        # add result to cache
        category_size_cache[category] = result

    return result