__author__ = 'Christoph'

import wikipedia
import tweepy
import pymysql
import logging
import application.input
import application.entity_linking
import application.aggregation
import application.article_recommender
import random
import time
from nltk import sent_tokenize

logging.basicConfig(level=logging.INFO, format="%(asctime)s: %(levelname)s %(message)s")
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

# Constants, global variables
category_filter = ["People", "Living people", "Humans", "People by status", "Year of birth missing (living people)",
                   "Human names", "Given names", "Given names by language", "European masculine given names",
                   "Masculine given names", "Names", "Personhood", "Society", "Social groups"]


def connect():
    con = None
    try:
        con = pymysql.connect(host='',
                              user='',
                              passwd='',
                              db='',
                              charset='utf8mb4',
                              cursorclass=pymysql.cursors.DictCursor)
    except Exception as e:
        logger.error("Error while establishing database connection")

    return con


def write_profile_to_database(profile_type, interests, user):
    connection = connect()
    if connection:
        # shuffle interests for unbiased evaluation
        random.shuffle(interests)
        for interest in interests:
            try:
                with connection.cursor() as cursor:
                    select_sql = "SELECT id FROM interest_categories WHERE interest_category=%s"
                    cursor.execute(select_sql, (interest[0]))
                    category_result = cursor.fetchone()
                    select_sql = "SELECT id FROM twitter_users WHERE twitter_id=%s"
                    cursor.execute(select_sql, (user.id))
                    user_result = cursor.fetchone()
                    if category_result and user_result:
                        category_id = category_result["id"]
                        user_id = user_result["id"]
                        input_sql = "INSERT INTO interest_profiles (user_id, profile_type_id, interest_category, weight) " \
                                    "VALUES (%s, %s, %s, %s)"
                        cursor.execute(input_sql, (user_id, profile_type, category_id, interest[1]))
                        connection.commit()
            except Exception as e:
                logger.error("Error while writing interest profile to database: " + str(e))
    connection.close()


def write_articles_to_database(articles):
    connection = connect()
    if connection:
        # shuffle articles for unbiased evaluation
        random.shuffle(articles)
        for article in articles:
            try:
                with connection.cursor() as cursor:
                    select_sql = "SELECT id FROM interest_categories WHERE interest_category=%s"
                    cursor.execute(select_sql, ("Page:" + str(article[0].title)))
                    result = cursor.fetchone()
                    if not result:
                        summary = article[0].summary
                        summary_sentences = sent_tokenize(summary)
                        if len(summary_sentences) > 5:
                            summary_text = " ".join(summary_sentences[:5])
                        else:
                            summary_text = " ".join(summary_sentences)
                        input_sql = "INSERT INTO interest_categories (interest_category, description) " \
                                    "VALUES (%s, %s)"
                        cursor.execute(input_sql, ("Page:" + str(article[0].title), summary_text))
                        connection.commit()
            except Exception as e:
                logger.error("Error while writing interest profile to database")
    connection.close()


def get_all_interests():
    connection = connect()
    interests = []

    if connection:
        try:
            with connection.cursor() as cursor:
                select_sql = "SELECT ic.interest_category FROM interest_profiles ip " \
                             "JOIN interest_categories ic ON ip.interest_category = ic.id LIMIT 2000"
                cursor.execute(select_sql)
                results = cursor.fetchall()
                for result in results:
                    interest = result["interest_category"]
                    if not interest.startswith("Page:"):
                        interests.append(interest)
        except Exception as e:
            logger.error("Error while writing interest profile to database")
    connection.close()

    # shuffle interests at the end and convert them to set
    random.shuffle(interests)
    return set(interests)


def generate_profiles():
    while True:
        # check for new twitter users with no generated interest profiles
        screen_name = None

        connection = connect()
        if connection:
            try:
                with connection.cursor() as cursor:
                    select_sql = 'SELECT id, screen_name FROM twitter_users WHERE profile_generated=0'
                    cursor.execute(select_sql)
                    result = cursor.fetchone()

                    if not result:
                        connection.close()
                        break
            except Exception as e:
                connection.rollback()
                logger.error("Error while fetching new users with no profile: " + str(e))
                break

            # complete user information with twitter
            screen_name = result["screen_name"]
            user = application.input.get_user(screen_name)
            if user:
                try:
                    with connection.cursor() as cursor:
                        update_sql = "UPDATE twitter_users SET twitter_id=%s, friends_count=%s, lang=%s WHERE screen_name=%s"
                        cursor.execute(update_sql, (user.id, user.friends_count, user.lang, screen_name))
                        connection.commit()
                        logger.info("Updated profile information for " + screen_name + " successfully")
                except Exception as e:
                    connection.rollback()
                    logger.error("Error while updating user information: " + str(e))
                    connection.close()
                    break
            else:
                connection.close()
                break
            connection.close()
        else:
            break

        # generate the profiles for the user and write them to database
        if user:
            print("Generate profile for user " + user.screen_name)
            # generate the profile type 1
            application.aggregation.infer_interests(screen_name, entity_heuristics=False, number_of_iterations=5,
                                                    decay_factor=0.2, normalization=True, page_intersection_boost=True)
            interests_type_1 = application.aggregation.get_top_interests(top=20, filter_by=category_filter)
            all_interests_type_1 = application.aggregation.get_top_interests(get_all=True, filter_by=category_filter)
            write_profile_to_database("1", interests_type_1, user)
            logger.info("Generated profile type 1 for " + screen_name)

            # generate the profile type 2
            application.aggregation.infer_interests(screen_name, entity_heuristics=True, number_of_iterations=5,
                                                    decay_factor=0.2, normalization=True, page_intersection_boost=False)
            top_interests_type_2 = application.aggregation.get_top_interests(top=20, filter_by=category_filter)
            all_interests_type_2 = application.aggregation.get_top_interests(get_all=True, filter_by=category_filter)
            write_profile_to_database("2", top_interests_type_2, user)
            logger.info("Generated profile type 2 for " + screen_name)

            # generate the profile type 3
            application.aggregation.infer_interests(screen_name, entity_heuristics=True, number_of_iterations=3,
                                                    decay_factor=0.2, normalization=True, page_intersection_boost=True)
            interests_type_3_input = application.aggregation.get_top_interests(top=10, filter_by=category_filter)

            # choose 5 top interests randomly and get an article for them
            articles = []
            try:
                for interest in random.sample(interests_type_3_input, 5):
                    article = application.article_recommender.get_random_wiki_page(interest[0], number_of_tries=10)
                    if article:

                        articles.append((article, interest[1]))
            except ValueError as e:
                logger.error("Error while choosing article sample " + str(e))

            # write articles to database and collect their interest categories id
            write_articles_to_database(articles)

            interests_type_3 = []
            for article in articles:
                interests_type_3.append(("Page:" + article[0].title, article[1]))
            write_profile_to_database("3", interests_type_3, user)
            logger.info("Generated profile type 3 for " + screen_name)

            # generate the profile type 4
            # get 10 interest categories in which the user is supposed to be not interested
            all_interests = all_interests_type_1 + all_interests_type_2
            uninteresting_category_names = random.sample(list(filter(lambda k: k not in [x[0] for x in all_interests]
                                                                     , get_all_interests())), 10)
            uninteresting_categories = []
            for uninteresting_category_name in uninteresting_category_names:
                uninteresting_categories.append((uninteresting_category_name, 0))

            # profile consists of top 10 interest from type2 and 10 uninteresting categories
            interests_type_4 = top_interests_type_2[:10]
            interests_type_4.extend(uninteresting_categories)
            write_profile_to_database("4", interests_type_4, user)
            logger.info("Generated profile type 4 for " + screen_name)

            # set survey as generated/ready
            connection = connect()
            if connection:
                try:
                    with connection.cursor() as cursor:
                        select_sql = 'UPDATE twitter_users SET profile_generated=1 WHERE twitter_id=%s'
                        cursor.execute(select_sql, (user.id))
                        connection.commit()
                except Exception as e:
                    connection.rollback()
                    logger.error("Error while fetching new users with no profile: " + str(e))
                    break
            connection.close()


while True:
    print("Check for new interest profiles to generate")
    generate_profiles()
    print("Done, sleeping 10 Minutes")
    time.sleep(10*60)
