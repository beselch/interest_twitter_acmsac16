"""
This module provides functions for retrieving the syntactic input for generating semantic interest profiles.
The output of this module is a list of plain text surface forms which must be linked to entities in order to
generate an interest profile.
"""
__author__ = 'Christoph'

import tweepy
import logging
import time

logger = logging.getLogger(__name__)

# Tweepy Account Constants
CONSUMER_KEY = ""
CONSUMER_SECRET = ""
ACCESS_TOKEN = ""
ACCESS_TOKEN_SECRET = ""

# global variables
twitter_friends_cache = {}  # cache for the friends of a user (twitter api rate limit)
api = None

# Caches
tweets_cache = {}

def init_api():
    global api

    # Create Authentication Handler, login, create Twitter API wrapper
    try:
        auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
        auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
        api = tweepy.API(auth_handler=auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
        logger.info("Successfully created twitter API")
    except tweepy.TweepError as e:
        logger.error("Failed to create Twitter API")


def fetch_twitter_friends(screenname, popular_filter=True):
    """Function returns the list of twitter friends to a given and valid twitter screen name.
    Returns an empty list if the given twitter screenname does not exist, the profile has no friends or is private.

    :param screenname: valid and existing screenname of a twitter user
    :return: list of user accounts for the specified user
    """

    twitter_friends = []
    global api
    global twitter_friends_cache

    # check cache for given screenname first
    if screenname in twitter_friends_cache:
        twitter_friends = list(twitter_friends_cache[screenname])
        logger.debug("Fetched friends for " + screenname + "from cache")
    else:
        # Get names of friends for specified user
        try:
            for user in tweepy.Cursor(api.friends, screen_name=screenname).items():
                if len(twitter_friends) > 500:
                    break
                # Only take users that are somehow "popular" into account, filter private users
                if popular_filter:
                    if user.followers_count > 100 or user.verified is True:
                        twitter_friends.append(user)
                time.sleep(5)

            # save retrieved friend list to cache for later use
            twitter_friends_cache[screenname] = list(twitter_friends)
            logger.info("Fetched  friends for " + screenname + "from twitter API")
        except tweepy.TweepError as e:
            logger.error("Failed to fetch twitter friends" + str(e))

    return twitter_friends


def fetch_tweets_text(screenname):
    """Function returns a string consisting the 20 most recent tweets of a user and the content of his self
    description box.
    If the user does not exist, the profile is private or has no tweets an empty string is returned.

    :param screenname: valid and existing screenname of a twitter user
    :return: text of the self description box and the 20 most recent tweets of the specified user
    """

    tweets_text = ""
    global api

    # check cache first
    if screenname in tweets_cache:
        tweets_text = tweets_cache[screenname]
        logger.debug("Fetched tweets for " + screenname + "from cache")
    else:
        try:
            tweets = api.user_timeline(screenname)
            for tweet in tweets:
                tweets_text = tweets_text + tweet.text
            logger.info("Fetched  friends for " + screenname + "from twitter API")
        except Exception as e:
            logger.error("Failed to fetch tweets")

        # write result to cache
        tweets_cache[screenname] = tweets_text

    return tweets_text


def get_search_suggestion(query):
    """Function gets the twitter account id of the first result of a twitter search on the given query.

    :param query: the query for twitter api account search
    :return: the user id of the first search result
    """

    first_result = 0
    global api

    try:
        search_res = api.search_users(query)
        if len(search_res) > 0:
            first_result = search_res[0].id
    except tweepy.TweepError as e:
        logger.error("Failed to perform search on twitter API")

    return first_result


def get_context(username):
    """Function returns the context of a twitter account name for disambiguating and linking it to the correct entity.

    :param username: valid and existing screenname of a twitter user
    :return: gets the context of twitter account name for word sense disambiguation
    """

    global api

    if api:
        return fetch_tweets_text(username)
    else:
        init_api()
        return fetch_tweets_text(username)


def get_surface_forms(username):
    """Function returns the list of surface forms for inferring the interests of a given user.

    :param username: valid and existing screenname of a twitter user
    :return: list of surface forms
    """
    global api

    if api:
        return fetch_twitter_friends(username)
    else:
        init_api()
        return fetch_twitter_friends(username)


def reverse_linking(query, user_id):
    """Function does a reverse linking based on a given search query.

    :param query: search query
    :param user_id: id of the user to compare with
    :return: result of reverse linking
    """
    global api

    if api:
        result = get_search_suggestion(query) == user_id
    else:
        init_api()
        result = get_search_suggestion(query) == user_id

    return result


def get_user(screenname):
    """Function returns the tweepy user object for a given twitter screenname

    :param screenname: twitter screenname
    :return: tweepy user object
    """
    global api
    result = None

    try:
        if api:
            result = api.get_user(screenname)
        else:
            init_api()
            result = api.get_user(screenname)
    except Exception as e:
        logger.error("Error occurred while retrieving twitter user")

    return result
