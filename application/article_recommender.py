__author__ = 'Christoph'
import requests
import urllib.parse
from mw.lib import title
import wikipedia

VIEW_COUNT_API_ENDPOINT = "http://stats.grok.se/json/en/latest30/"
RANDOM_PAGE_FORM = "https://en.wikipedia.org/wiki/Special:RandomInCategory"


def get_random_wiki_page(category, number_of_tries=5):
    random_pages = []
    params = {"wpcategory": category}

    for i in range(0, number_of_tries):
        try:
            # get random wiki page of category
            res = requests.post(RANDOM_PAGE_FORM, params)
            random_page_url = urllib.parse.urlsplit(res.url)[2].split("/")[2]
            random_page = wikipedia.page(title.normalize(urllib.parse.unquote(random_page_url)))

            # get page view counts
            page_views = 0
            res = requests.get(VIEW_COUNT_API_ENDPOINT + random_page_url)
            res_json = res.json()
            res_dict = dict(res_json["daily_views"])
            for key in res_dict:
                page_views += res_dict[key]

            random_pages.append((random_page, page_views))
        except:
            pass

    result = max(random_pages, key=lambda x: x[1])
    if len(result) > 0:
        return result[0]
    else:
        return None
