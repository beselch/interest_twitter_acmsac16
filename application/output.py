__author__ = 'Christoph'

import json
import logging
from networkx.readwrite import json_graph
import application.aggregation as aggregation
import application.entity_linking as entity_linking


logger = logging.getLogger(__name__)


def print_interests(interests):
    """Functions prints the interests including their weight in a table.

    :param interests: list of interset-weight tuples
    """
    for interest in interests:
        print("{0:20} ({0:.4f})".format(interest[0], interest[1]))


def export_interests_to_json(username, interests):
    """Functions encodes the user interests and weights to JSON format.

    :param username: name of user which interests are provided
    :param interests: interests of the user
    :return: interest profile for user in JSON format
    """
    result = dict(user=username, interests=[])

    for interest in interests:
        interest_entry = dict(interset=interest[0], weight=interest[1])
        result["intersets"].append(interest_entry)

    return json.dump(result)


def export_graph_to_json(graph, file):
    """Function exports initial or activated graph/taxonomy to JSON format.

    :param graph: networkx graph
    :return: JSON encoded category taxonomy
    """
    # Clean the graph from internal data
    for node in graph.nodes_iter(data=True):
        if "entities" in node[1]:
            node[1].pop("entities", None)

    # Convert to graph data to JSON
    data = json_graph.node_link_data(graph)

    # Write result with JSON dump to outputfile
    with open(file, "w") as outfile:
        json.dump(data, outfile)


aggregation.infer_interests("bonzo1993")
