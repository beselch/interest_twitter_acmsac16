import tweepy
import requests
import rdflib
import wikipedia
import networkx as nx


## Constants
# Tweepy/Twitter API
CONSUMER_KEY = "WOywcaF0aOx4GVQ6nRllWpIcw"
CONSUMER_SECRET = "I2LBofgD9wDr1ZLhC2lqkqMhZ93jImMIuwyDN1Zn0I0Ga0rLhs"
ACCESS_TOKEN = "68679834-OZaXicDhgLC4UK5uRqNivWBDJqWHWNG7NNKu72jxV"
ACCESS_TOKEN_SECRET = "jOdpR5KEyYIomitszFmzLcUZs72chYM80IcPVD0uyEDVa"
# Wibi web service
WIBI_WEB_SERVICE_ENABLED = False     # Use WiBi webservice to fetch categories of page
WIBI_PAGE_HEIGHT = 1
WIBI_CATEGORY_HEIGHT = 10
# Spreading Activation Algorithm
NUMBER_OF_ITERATIONS = 5
DECAY_FACTOR = 0.2

# Global variables
twitter_friends_cache = {}
wikipedia_categories_cache = {}

# Function returns the list of twitter friends to a given and valid twitter screen name
# Returns an empty list if the given twitter screenname does not exist or the profil is not public
def fetch_twitter_friends(screenname):
    twitter_friends = []

    # Create Authentification Handler, login, create Twitter API wrapper
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth_handler=auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    #check cache for given screenname first
    #friendlist_hash = hash(str(api.friends_ids(screenname)))
    if screenname in twitter_friends_cache:
        twitter_friends = list(twitter_friends_cache[screenname])
    else:
        # Get names of friends for specified user
        for user in tweepy.Cursor(api.friends, screen_name=screenname).items():
            twitter_friends.append(user.name)

        twitter_friends_cache[screenname] = list(twitter_friends)

    return twitter_friends


# Function returns the list of all wikipedia categories the given page is assigned to
# Returns an empty list if no page with the given name does exist
def fetch_wikipedia_categories(page_name):
    categories = []

    #check cache first
    if page_name in wikipedia_categories_cache:
        categories = wikipedia_categories_cache[page_name]
    else:
        if(WIBI_WEB_SERVICE_ENABLED):
            # Use WIBI Webservice for fetching categories of pages
            rdf_graph = rdflib.Graph()
            wibi_rdf_export = requests.get("http://wibitaxonomy.org/rdf/?type=page&item=" + page_name.replace(" ", "_") +
                                "&pageH=" + str(WIBI_PAGE_HEIGHT) + "&cateH=" + str(WIBI_CATEGORY_HEIGHT) + "&output=xml")
            if(wibi_rdf_export.content):
                rdf_graph.parse(data=wibi_rdf_export.content, format="xml")

            # IMPROVE SPARQL query here, use only the categories assigned to the specified page name
            wiki_categories = rdf_graph.query("SELECT DISTINCT ?aname ?bname WHERE {?a wibi-model:hasWikipediaCategory ?bname}")
            for category in wiki_categories:
                categories.append((category[1].split(":"))[2])
        else:
            # Use Wikipedia API for fetching categories of pages
            try:        
                wikipedia_page = wikipedia.page(page_name)
                categories = wikipedia_page.categories
            except:
                pass

            # Remove irrelevant categories (e.g. admin categroies) from list
            irrelevant_categories = []
            with open("resources\irrelevant-wiki-categories.tsv") as file:
                for line in file.readlines():
                    irrelevant_categories.append(line.rstrip('\n'))
            
            categories = [category for category in categories if category not in irrelevant_categories]
            wikipedia_categories_cache[page_name] = categories.copy()

    return categories


# Returns the wikipedia category taxonomy
# Returns an empty graph if the taxonomy files/resources (edgelist) could not be found
def generate_category_taxonomy():
    graph = nx.Graph()

    graph = nx.read_edgelist("taxonomies\WiBi.categorytaxonomy.ver1.0.txt", delimiter="\t", nodetype=str, create_using=nx.DiGraph())
    # Paper: Hierarchical Interests
    #graph = nx.read_weighted_edgelist("taxonomies\categories-removed.txt", delimiter="\t", nodetype=str, create_using=nx.DiGraph())


    return graph


# Functions calculates user interets for a given twitter screenname based on his friendslist and the wikipedia category hierarchy
# Returns list of interets (top number of interets)
def infer_interests(screenname, top):
    interests = []
    
    # get list of twitter friends for specified user
    print("Fetching list of twitter friends...")
    twitter_friends = fetch_twitter_friends(screenname)

    # get the (merged) list of all wikipedia categories that could be
    # directly associated with the twitter friends of the specified user
    print("Fetching the corresponding list of pages and assigend wikipedia categories...")
    wikipedia_categories = []
    for friend_name in twitter_friends:
        wikipedia_categories.extend(fetch_wikipedia_categories(friend_name))

    # get the wikipedia category taxonomy
    print("Generate the wikipedia taxonomy graph...")
    category_taxonomy = generate_category_taxonomy()

    # initially activate all category nodes which could be directly
    # inferred of the users friend list
    for category in wikipedia_categories:
        if category_taxonomy.has_node(category):
            if not "activation" in category_taxonomy.node[category]:
                category_taxonomy.node[category]["activation"] = 1.0
            else:
                category_taxonomy.node[category]["activation"] += 1.0

    # start spreading activation from starting nodes
    print("Execute the spreading activation algorithm...")
    activated_nodes = []
    for i in range(1, NUMBER_OF_ITERATIONS):
        # Get all activated nodes
        for node in category_taxonomy.nodes_iter(data=True):
            if bool(node[1]):
                activated_nodes.append(node)

        for node in activated_nodes:
            successors = category_taxonomy.successors(node[0])
            for successor in successors:
                if not "activation" in category_taxonomy.node[successor]:
                    category_taxonomy.node[successor]["activation"] = DECAY_FACTOR * node[1]["activation"] #* (1/(math.log(category_taxonomy.in_degree(successor)+1)))
                else:
                    category_taxonomy.node[successor]["activation"] += DECAY_FACTOR * node[1]["activation"] #* (1/(math.log(category_taxonomy.in_degree(successor)+1)))


    # Get all activated nodes and sort them by activation level
    print("Finish and prepare data...")
    activated_nodes = []
    for node in category_taxonomy.nodes_iter(data=True):
        if bool(node[1]):
            # one penalty on the end
            #node[1]["activation"] = node[1]["activation"] * (1/(math.log(category_taxonomy.in_degree(node[0])+1)+1))
            activated_nodes.append(node)
    activated_nodes.sort(key=lambda k: k[1]["activation"], reverse=True)

    # Get the names of the most activated categories (top x) as interets
    node_names = []
    for node in activated_nodes:
        node_names.append((node[0], node[1]["activation"]))
    interests = node_names[:top]
    
    return interests

print(infer_interests("TinaEcker", 50))